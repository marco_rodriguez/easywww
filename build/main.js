webpackJsonp([10],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CercaDeMiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CercaDeMiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CercaDeMiPage = /** @class */ (function () {
    function CercaDeMiPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CercaDeMiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CercaDeMiPage');
    };
    CercaDeMiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cerca-de-mi',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/cerca-de-mi/cerca-de-mi.html"*/'<!--\n  Generated template for the CercaDeMiPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar>\n	<ion-title>Cerca De Mi</ion-title>\n	</ion-navbar>\n</ion-header>\n\n\n<ion-content class="cards-bg img-fondo">\n	<div class="adv-map">\n		<div style="position: auto">\n			<img src="">\n			<ion-fab right top>\n				<button ion-fab color="secondary">\n					<ion-icon name="pin"></ion-icon>\n				</button>\n			</ion-fab>\n		</div>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/cerca-de-mi/cerca-de-mi.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], CercaDeMiPage);
    return CercaDeMiPage;
}());

//# sourceMappingURL=cerca-de-mi.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormularioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mis_alquileres_mis_alquileres__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FormularioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FormularioPage = /** @class */ (function () {
    function FormularioPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FormularioPage.prototype.misAlquileres = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__mis_alquileres_mis_alquileres__["a" /* MisAlquileresPage */]);
    };
    FormularioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-formulario',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/formulario/formulario.html"*/'<!--\n  Generated template for the FormularioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title class="nav-color"><img class="logo-header" src="../../assets/imgs/EasyParking-logo.png" alt="">Easy Parking</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n <ion-grid>\n	<ion-card>\n\n	  <ion-card-header>\n	    <p class="title-form">Completa el <br> siguiente formulario</p>\n	  </ion-card-header>\n\n	  <ion-card-content>\n	     <form>\n	      <ion-item>\n	        <ion-label stacked>Calle</ion-label>\n	        <ion-input class="caja-form" type="text" name="title"></ion-input>\n	      </ion-item>\n	      <ion-item >\n	        <ion-label stacked>Número</ion-label>\n	        <ion-input class="caja-form" type="text" name="title"></ion-input>\n	      </ion-item>\n	      <ion-item >\n	        <ion-label stacked>Barrio</ion-label>\n	        <ion-input class="caja-form" type="text" name="title"></ion-input>\n	      </ion-item>\n	      <ion-item >\n	        <ion-label stacked>Teléfono</ion-label>\n	        <ion-input class="caja-form" type="text" name="title"></ion-input>\n	      </ion-item>\n	      <ion-item>\n	        <ion-label stacked>Descripción General</ion-label>\n	        <ion-textarea class="caja-form" name="description"></ion-textarea>\n	      </ion-item>  \n	            \n           <div class="item-foto"> \n      		 <ion-label stacked>Cargar Fotos</ion-label>\n	        <ol type=”1” start=”3”>\n				<li> Lorem ipsum dolor sit amet</li>\n				<li> Lorem ipsum dolor sit amet</li>\n				<li> Lorem ipsum dolor sit amet</li>\n			</ol>\n           </div> \n           \n			    <ion-row>\n			      <ion-col col-1>\n			        <div class="caja-adjuntar-1">\n			        	 <a class="ancla-adjuntar" href=""><ion-icon class="color-image" name="image"></ion-icon><span class="span-adjuntar">Adjuntar</span></a>\n			        </div>\n			      </ion-col>\n			      <ion-col offset-3 col-1>\n				    <div class="caja-adjuntar-2">\n				        <a class="ancla-adjuntar" href=""><ion-icon class="color-image" name="image"></ion-icon><span class="span-adjuntar">Adjuntar</span></a>\n				    </div>\n				   </ion-col>\n				  <ion-col offset-3 col-1>\n				    <div class="caja-adjuntar-3">\n				        <a class="ancla-adjuntar" href=""><ion-icon class="color-image" name="image"></ion-icon><span class="span-adjuntar">Adjuntar</span></a>\n				    </div>\n				  </ion-col>\n			    </ion-row>\n			    <ion-row>\n			      <ion-col offset-4 col-8>\n				    <div class="">\n				        <a class="" href=""><ion-icon class="ico-mas-foto" name="add"><span class="span-mas-foto">Más fotos</span></ion-icon></a>\n				    </div>\n				  </ion-col>\n			    </ion-row>\n\n            \n\n           <div>\n		     <button class="butt-login-1" ion-button ful color="danger" block (click)="misAlquileres()">Enviar</button>\n		   </div>\n		 </form>\n	  </ion-card-content>\n\n	</ion-card>\n </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/formulario/formulario.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], FormularioPage);
    return FormularioPage;
}());

//# sourceMappingURL=formulario.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MetodoPagoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tab_icon_text_content_tab_icon_text_content__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the MetodoPagoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MetodoPagoPage = /** @class */ (function () {
    function MetodoPagoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MetodoPagoPage.prototype.tabIcon = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tab_icon_text_content_tab_icon_text_content__["a" /* TabIconTextContentPage */]);
    };
    MetodoPagoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-metodo-pago',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/metodo-pago/metodo-pago.html"*/'<!--\n  Generated template for the FormularioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title class="nav-color"><img class="logo-header" src="../../assets/imgs/EasyParking-logo.png" alt="">Easy Parking</ion-title>\n	</ion-navbar>\n\n	<div class="div-header-1">\n		<ion-grid>\n			<ion-row>\n				<ion-col offset-4 col-sm-4 col-md-4 col-lg-4>	\n					<span class="span-monto-pagar">Monto a pagar:</span><span class="span-monto"> 100$</span>\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n	</div>\n</ion-header>\n\n\n<ion-content padding>\n	<ion-grid>\n		<div class="select-pago">\n			<img class="img-pago" src="../../assets/imgs/svg/give-money.svg" alt=""><br>\n			<span class="span-metodo-pago">Selecciona <br></span><span class="span-metodo-pago-2">Método de pago</span>\n		</div>\n\n		<ion-card class="cards-list-demo">\n			<ion-list>\n				<button ion-item>\n					<img class="icono-dolar" src="../../assets/imgs/Icono-Dolar.png" alt="" item-start>\n					<span class="titulo-lista-pago-1">Transferencia Bancaria</span> <br><small class="small-lista-pago-1">Por número de cuenta</small>\n				</button>\n\n				<button ion-item>\n					<img class="icono-pago" src="../../assets/imgs/Icono-Pago.png" alt="" item-start>\n					<span class="titulo-lista-pago-2">Pago Mis Cuentas<br><small>Pagos online</small></span>\n				</button>\n\n				<button ion-item>\n					<img class="icono-tarjeta" src="../../assets/imgs/icono-tarjeta.png" alt="" item-start>\n					<span class="titulo-lista-pago-3">Tarjeta de crédito/débito <br><small class="small-lista-pago-3">Visa, Mastercard y American Express</small></span>\n				</button>\n\n				<button ion-item>\n					<img class="icono-mercado" src="../../assets/imgs/Icono-mercadopago.png" alt="" item-start>\n					<span class="titulo-lista-pago-4">MercadoPago<br><small class="small-lista-pago-4">Tarjetas de crédito/débito/recargable </small></span>\n				</button>\n\n				<button ion-item>\n					<img class="icono-todopago" src="../../assets/imgs/icono-todopago.png" alt="" item-start>\n					<span class="titulo-lista-pago-5">Todo Pago<br><small class="small-lista-pago-5">Tarjetas de crédito/débito/recargable </small></span>\n				</button>\n\n			</ion-list>\n			 \n		</ion-card>\n		\n		    <div>\n		     <button (click)="tabIcon()" class="butt-login-1" ion-button ful color="danger" block>Continuar</button>\n		    </div>\n		<!-- fin de lacard -->\n\n\n	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/metodo-pago/metodo-pago.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], MetodoPagoPage);
    return MetodoPagoPage;
}());

//# sourceMappingURL=metodo-pago.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RankingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RankingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RankingPage = /** @class */ (function () {
    function RankingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RankingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RankingPage');
    };
    RankingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-ranking',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/ranking/ranking.html"*/'<!--\n  Generated template for the RankingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title>Ranking</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-card>\n         <ion-item class="card-ranking">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-sm-1 col-md-1 col-lg-1>\n                <div class="numero-ranking">1</div>\n              </ion-col>\n              <ion-col col-sm-3 col-md-3 col-lg-3 >\n                <ion-avatar item-start>\n                 <img src="../../assets/imgs/parking.jpg">\n               </ion-avatar>\n             </ion-col>\n             <ion-col col-sm-8 col-md-8 col-lg-8>\n              <div class="div-sub-titulo-ranking"><p class="sub-titulo-ranking">Autorizaciones</p>\n               <ion-badge color="danger" class="badge-ranking">Disponibilidad 24 hrs</ion-badge><br><span class="direccion">Av. Juarez 3456 Miraflores</span></div>\n             </ion-col>\n           <!--   <ion-col>\n              <div class="div-comentario">\n               <ion-icon class="ico-comentario"  name="create" large ></ion-icon>\n               <p class="span-comentario">Comentarios</p>\n             </div>\n           </ion-col> -->\n\n         </ion-row>\n\n\n       </ion-grid>\n\n\n     </ion-item>\n\n     <div class="div-ranking">\n\n      <ion-grid>\n        <ion-row>\n          <ion-col offset-1 class="span-div-ranking">\n           Precio por estadia: 40$\n         </ion-col>\n         <ion-col  class="span-div-ranking" >\n          Precio por hora: 20$\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n\n  </div>\n</ion-card>\n</ion-col>\n</ion-row>\n<ion-row>\n      <ion-col>\n        <ion-card>\n         <ion-item class="card-ranking">\n          <ion-grid>\n            <ion-row>\n              <ion-col col-sm-1 col-md-1 col-lg-1>\n                <div class="numero-ranking">2</div>\n              </ion-col>\n              <ion-col col-sm-3 col-md-3 col-lg-3 >\n                <ion-avatar item-start>\n                 <img src="../../assets/imgs/parking.jpg">\n               </ion-avatar>\n             </ion-col>\n             <ion-col col-sm-8 col-md-8 col-lg-8>\n              <div class="div-sub-titulo-ranking"><p class="sub-titulo-ranking">Autorizaciones</p>\n               <ion-badge color="danger" class="badge-ranking">Disponibilidad 24 hrs</ion-badge><br><span class="direccion">Av. Juarez 3456 Miraflores</span></div>\n             </ion-col>\n           <!--   <ion-col>\n              <div class="div-comentario">\n               <ion-icon class="ico-comentario"  name="create" large ></ion-icon>\n               <p class="span-comentario">Comentarios</p>\n             </div>\n           </ion-col> -->\n\n         </ion-row>\n\n\n       </ion-grid>\n\n\n     </ion-item>\n\n     <div class="div-ranking">\n\n      <ion-grid>\n        <ion-row>\n          <ion-col offset-1 class="span-div-ranking">\n           Precio por estadia: 40$\n         </ion-col>\n         <ion-col  class="span-div-ranking" >\n          Precio por hora: 20$\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n\n  </div>\n</ion-card>\n</ion-col>\n</ion-row>\n</ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/ranking/ranking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], RankingPage);
    return RankingPage;
}());

//# sourceMappingURL=ranking.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScannerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { BarcodeScanner } from '@ionic-native/barcode-scanner';
/**
 * Generated class for the ScannerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ScannerPage = /** @class */ (function () {
    function ScannerPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ScannerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-scanner',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/scanner/scanner.html"*/'<!--\n  Generated template for the ScannerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Scanner</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="card-background-page codigo-qr">\n\n  <ion-card class=" img-codigo-qr">\n    <img src="../../assets/imgs/qr.png"/>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/scanner/scanner.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], ScannerPage);
    return ScannerPage;
}());

// this.barcodeScanner.scan().then(barcodeData => {
//  		console.log('Barcode data', barcodeData);
//  	}).catch(err => {
//  		console.log('Error', err);
//  	});
//# sourceMappingURL=scanner.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurarPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RestaurarPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RestaurarPasswordPage = /** @class */ (function () {
    function RestaurarPasswordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RestaurarPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RestaurarPasswordPage');
    };
    RestaurarPasswordPage.prototype.regresarLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    RestaurarPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-restaurar-password',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/restaurar-password/restaurar-password.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Restaurar Contraseña\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <img src="../../assets/imgs/img-EasyParking-1.png"/>\n  <form>\n    <ion-list inset>\n\n      <ion-item class="reset">\n        <ion-icon class="ico-mail" name="mail" item-start></ion-icon>\n        <ion-label floating>Ingresar correo registrado</ion-label>\n        <ion-input class="" type="text"  required></ion-input>\n      </ion-item>\n      <ion-item>\n        \n        \n        \n      </ion-item>\n      \n    </ion-list>\n    <div class="butt-reset">\n     <button (click)="regresarLogin()"  class="butt-login-1" ion-button ful color="primary" block>Enviar</button>\n     <button class="butt-login-1" ion-button ful color="secondary" block>Cancelar</button>\n     \n   </div>\n </form>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/restaurar-password/restaurar-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], RestaurarPasswordPage);
    return RestaurarPasswordPage;
}());

//# sourceMappingURL=restaurar-password.js.map

/***/ }),

/***/ 118:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 118;

/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/agradecimiento/agradecimiento.module": [
		301,
		9
	],
	"../pages/cerca-de-mi/cerca-de-mi.module": [
		302,
		8
	],
	"../pages/formulario/formulario.module": [
		303,
		7
	],
	"../pages/metodo-pago/metodo-pago.module": [
		304,
		6
	],
	"../pages/ranking/ranking.module": [
		305,
		5
	],
	"../pages/registro/registro.module": [
		306,
		4
	],
	"../pages/restaurar-password/restaurar-password.module": [
		307,
		3
	],
	"../pages/scanner/scanner.module": [
		308,
		2
	],
	"../pages/select-tiempo/select-tiempo.module": [
		309,
		1
	],
	"../pages/terminos/terminos.module": [
		310,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 160;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MisAlquileresPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mis_publicaciones_mis_publicaciones__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the MisPublicacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MisAlquileresPage = /** @class */ (function () {
    function MisAlquileresPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.monto = "monto1";
        this.event = {
            timeIni: '1990-02-19',
            timeEnds: '1990-03-20'
        };
        this.musicAlertOpts = {
            title: '1994 Music',
            subTitle: 'Select your favorite'
        };
    }
    MisAlquileresPage.prototype.stpSelect = function () {
        console.log('STP selected');
    };
    MisAlquileresPage.prototype.misPublicaciones = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__mis_publicaciones_mis_publicaciones__["a" /* MisPublicacionesPage */]);
    };
    MisAlquileresPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/mis-alquileres/mis-alquileres.html"*/'<!--\n  Generated template for the FormularioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title class="nav-color">Mis Alquileres</ion-title>\n	</ion-navbar>\n\n	<div class="div-header-1">\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6 col-sm>\n						<ion-item class="color-item">\n							<ion-label  class="color-label">Desde</ion-label>\n							<ion-datetime class="color-fecha" displayFormat="MMM DD YYYY" [(ngModel)]="event.timeIni"></ion-datetime>\n						</ion-item>\n					\n				</ion-col>\n\n				<ion-col col-6 col-sm>\n					\n						<ion-item class="color-item">\n							<ion-label class="color-label">Hasta</ion-label>\n							<ion-datetime class="color-fecha" displayFormat="MMM DD YYYY" [(ngModel)]="event.timeEnds"></ion-datetime>\n						</ion-item>\n				\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n	</div>\n\n</ion-header>\n\n<ion-content padding>\n	<ion-grid>\n		<ion-card>\n			<ion-card-header class="header-card">\n				<span class="fecha-header">Feb 19 1990</span>\n			</ion-card-header>\n\n			<ion-card-content>\n				<div class="separador-div"><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n				<div class="separador-div"><span class="titulo-separador-2">Dorrengo 300, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n\n				<div class="separador-div"><span class="titulo-separador-2">Perón 335, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n\n\n\n			</ion-card-content>\n\n		</ion-card> \n		<!-- fin de la primera card -->\n		<!-- Inicio de la segunda card -->\n		\n		<ion-card>\n\n			<ion-card-header class="header-card">\n				<span class="fecha-header">Feb 19 1990</span>\n			</ion-card-header>\n\n			<ion-card-content>\n				<div class="separador-div"><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n				<div class="separador-div"><span class="titulo-separador-2">Dorrengo 300, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n\n				<div class="separador-div"><span class="titulo-separador-2">Perón 335, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n				<div>\n		     <button class="butt-login-1" ion-button ful color="danger" block (click)="misPublicaciones()">Continuar</button>\n		   </div>\n\n\n\n			</ion-card-content>\n\n		</ion-card> \n		<!-- fin de la segunda card -->\n	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/mis-alquileres/mis-alquileres.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], MisAlquileresPage);
    return MisAlquileresPage;
}());

//# sourceMappingURL=mis-alquileres.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MisPublicacionesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__metodo_pago_metodo_pago__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the MisPublicacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MisPublicacionesPage = /** @class */ (function () {
    function MisPublicacionesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.monto = "monto1";
        this.event = {
            timeIni: '1990-02-19',
            timeEnds: '1990-03-20'
        };
        this.musicAlertOpts = {
            title: '1994 Music',
            subTitle: 'Select your favorite'
        };
    }
    MisPublicacionesPage.prototype.metodoPago = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__metodo_pago_metodo_pago__["a" /* MetodoPagoPage */]);
    };
    MisPublicacionesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/mis-publicaciones/mis-publicaciones.html"*/'<!--\n  Generated template for the FormularioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title class="nav-color">Mis Publicaciones</ion-title>\n	</ion-navbar>\n\n	<div class="div-header-1">\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6 col-sm>\n						<ion-item class="color-item">\n							<ion-label  class="color-label">Desde</ion-label>\n							<ion-datetime class="color-fecha" displayFormat="MMM DD YYYY" [(ngModel)]="event.timeIni"></ion-datetime>\n						</ion-item>\n					\n				</ion-col>\n\n				<ion-col col-6 col-sm>\n					\n						<ion-item class="color-item">\n							<ion-label class="color-label">Hasta</ion-label>\n							<ion-datetime class="color-fecha" displayFormat="MMM DD YYYY" [(ngModel)]="event.timeEnds"></ion-datetime>\n						</ion-item>\n				\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n	</div>\n	<div class="div-header-2">\n		<ion-grid>\n			<ion-row>\n				<ion-col col-5 col-sm>\n						<ion-item class="color-item">\n							<ion-label  class="color-label">Mayo 2018: $ 100 </ion-label>\n						\n						</ion-item>\n					\n				</ion-col>\n\n				<ion-col col-7 col-sm>\n					\n						<ion-item class="color-item">\n							<ion-label class="color-label">Semana 1</ion-label>\n							<ion-select  [(ngModel)]="monto">\n								<ion-option value="monto1">$ 100</ion-option>\n								<ion-option value="monto2">$ 250</ion-option>\n								<ion-option value="monto3">$300</ion-option>\n\n							</ion-select>\n						</ion-item>\n				\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n	</div>\n\n\n\n\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-grid>\n\n		<ion-card>\n\n			<ion-card-header class="header-card">\n				<span class="fecha-header">Feb 19 1990</span>\n			</ion-card-header>\n\n			<ion-card-content>\n				<div class="separador-div"><span class="titulo-separador">José M.</span><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n				<div class="separador-div"><span class="titulo-separador">José M.</span><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n\n				<div class="separador-div"><span class="titulo-separador">José M.</span><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n\n\n\n			</ion-card-content>\n\n		</ion-card> \n		<!-- fin de la primera card -->\n		<!-- Inicio de la segunda card -->\n		<ion-card>\n\n			<ion-card-header class="header-card">\n				<span class="fecha-header">Feb 19 1990</span>\n			</ion-card-header>\n\n			<ion-card-content>\n				<div class="separador-div"><span class="titulo-separador">José M.</span><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n				<div class="separador-div"><span class="titulo-separador">José M.</span><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n\n				<div class="separador-div"><span class="titulo-separador">José M.</span><span class="titulo-separador-2">Viamonte 5000, CABA</span></div>\n\n				<ion-row>\n					<ion-col col-sm-5 col-md-5 col-lg-5>\n						<div class="linea-vertical">\n							<span class="span-titulo">Tiempo Alquilado</span><br><span class="span-subtitulo">2Hrs media</span>\n						</div>\n					</ion-col>\n\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div class="linea-vertical">\n							<span class="span-titulo">Método de pago</span><br><span class="span-subtitulo">Efectivo</span>\n						</div>\n					</ion-col>\n\n					<ion-col col-sm-3 col-md-3 col-lg-3>\n						<div>\n							<span class="span-titulo-3">Monto pagado</span><br><span class="span-subtitulo-3">$ 30</span>\n						</div>\n					</ion-col>\n				</ion-row>\n				<div>\n		     <button class="butt-login-1" ion-button ful color="danger" block (click)="metodoPago()">Continuar</button>\n		   </div>\n\n			</ion-card-content>\n\n		</ion-card>\n		<!-- fin de la segunda card -->\n	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/mis-publicaciones/mis-publicaciones.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], MisPublicacionesPage);
    return MisPublicacionesPage;
}());

//# sourceMappingURL=mis-publicaciones.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabIconTextContentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__cerca_de_mi_cerca_de_mi__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ranking_ranking__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__scanner_scanner__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabIconTextContentPage = /** @class */ (function () {
    function TabIconTextContentPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__scanner_scanner__["a" /* ScannerPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__cerca_de_mi_cerca_de_mi__["a" /* CercaDeMiPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__ranking_ranking__["a" /* RankingPage */];
    }
    TabIconTextContentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/tab-icon-text-content/tab-icon-text-content.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Scanner" tabIcon="qr-scanner"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Cerca de mi" tabIcon="navigate"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Ranking" tabIcon="podium"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/tab-icon-text-content/tab-icon-text-content.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabIconTextContentPage);
    return TabIconTextContentPage;
}());

//# sourceMappingURL=tab-icon-text-content.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgradecimientoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AgradecimientoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AgradecimientoPage = /** @class */ (function () {
    function AgradecimientoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AgradecimientoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AgradecimientoPage');
    };
    AgradecimientoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-agradecimiento',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/agradecimiento/agradecimiento.html"*/'<!--\n  Generated template for the FormularioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title class="nav-color"><img class="logo-header" src="../../assets/imgs/EasyParking-logo.png" alt="">Easy Parking</ion-title>\n	</ion-navbar>\n\n	\n</ion-header>\n\n\n<ion-content padding>\n	<ion-grid>\n		\n		<ion-card class="">\n\n			<ion-row>\n				<ion-col offset-sm-4 col-sm-2 col-md-2 offset-md-6 col-lg-2 offset-lg-6 >\n\n					<ion-icon class="img-tiempo"  name="alarm"></ion-icon>	\n\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col offset-3 col-sm-4 col-md-4 col-lg-4>\n					<div class="select-pago">		\n						<span class="span-select-tiempo">Selecciona</span><br><span class="span-tiempo">Tiempo</span>\n					</div>\n				</ion-col>\n			</ion-row>\n			<form>\n				\n				<ion-item class="color-textarea">\n					<ion-label floating>Dejanos tu comentario...</ion-label>\n					<ion-textarea name="description"></ion-textarea>\n				</ion-item>\n				<ion-item>\n					<ion-textarea placeholder="Enter a description"></ion-textarea>\n				</ion-item>\n\n				<button class="butt-login-1" ion-button ful color="danger" block>Finalizar</button>\n			</form>		\n\n		</ion-card>\n\n		<!-- fin de la card -->\n\n\n	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/agradecimiento/agradecimiento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AgradecimientoPage);
    return AgradecimientoPage;
}());

//# sourceMappingURL=agradecimiento.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegistroPage = /** @class */ (function () {
    function RegistroPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RegistroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegistroPage');
    };
    RegistroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-registro',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/registro/registro.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Registro De Usuario\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <img src="../../assets/imgs/img-EasyParking-1.png"/>\n  <form>\n\n   <ion-list inset>\n     <ion-item>\n      <ion-icon class="ico-mail" name="mail" item-start></ion-icon>\n      <ion-label floating>Correo</ion-label>\n      <ion-input type="text"  required></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon class="ico-lock" name="lock" item-start></ion-icon>\n      <ion-label floating>Contraseña</ion-label>\n      <ion-input type="Password"  required></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon class="ico-lock" name="lock" item-start></ion-icon>\n      <ion-label floating>Repetir Contraseña</ion-label>\n      <ion-input type="Password"  required></ion-input>\n    </ion-item>\n  </ion-list>\n  <div>\n   <button class="butt-login-1" ion-button ful color="primary" block>REGISTRARSE</button>\n   <a href="../login/login.html" class="butt-login-2" ion-button ful color="secondary" block>Cancelar</a>\n </div>\n</form>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/registro/registro.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], RegistroPage);
    return RegistroPage;
}());

//# sourceMappingURL=registro.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectTiempoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the SelectTiempoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SelectTiempoPage = /** @class */ (function () {
    function SelectTiempoPage() {
        this.gaming = "n64";
        this.gender = "f";
        this.musicAlertOpts = {
            title: '1994 Music',
            subTitle: 'Select your favorite'
        };
    }
    SelectTiempoPage.prototype.stpSelect = function () {
        console.log('STP selected');
    };
    SelectTiempoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-select-tiempo',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/select-tiempo/select-tiempo.html"*/'<!--\n  Generated template for the FormularioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar>\n		<ion-title class="nav-color"><img class="logo-header" src="../../assets/imgs/EasyParking-logo.png" alt="">Easy Parking</ion-title>\n	</ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n	<ion-grid>\n	   <ion-card class="">\n		<ion-row>\n			<ion-col offset-sm-4 col-sm-2 col-md-2 offset-md-6 col-lg-2 offset-lg-6>	\n				 <ion-icon class="img-tiempo" name="alarm"></ion-icon>	\n			</ion-col>\n		</ion-row>\n		<ion-row>\n			<ion-col offset-3 col-sm-4 col-md-4 col-lg-4>\n				<div class="select-pago">		\n					<span class="span-select-tiempo">Selecciona</span><br><span class="span-tiempo">Tiempo</span>\n				</div>\n			</ion-col>\n		</ion-row>\n			<div>\n				<ion-list class="list-select">\n				  <ion-item class="color-select">\n				    <ion-label>Disponibilidad</ion-label>\n				    <ion-select  flo class="select-text-2" [(ngModel)]="gaming">\n				      <ion-option value="nes">NES</ion-option>\n				      <ion-option value="n64">Nintendo64</ion-option>\n				      <ion-option value="ps">PlayStation</ion-option>\n				      <ion-option value="genesis">Sega Genesis</ion-option>\n				      <ion-option value="saturn">Sega Saturn</ion-option>\n				      <ion-option value="snes">SNES</ion-option>\n				    </ion-select>\n				  </ion-item>\n				</ion-list>\n			</div>	\n			<div>\n			<button class="butt-select-tiempo" ion-button ful color="danger" block>Siguiente</button>\n		</div>		\n\n		</ion-card>\n		\n		<!-- fin de lacard -->\n\n\n	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/select-tiempo/select-tiempo.html"*/,
        }),
        __metadata("design:paramtypes", [])
    ], SelectTiempoPage);
    return SelectTiempoPage;
}());

//# sourceMappingURL=select-tiempo.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TerminosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TerminosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TerminosPage = /** @class */ (function () {
    function TerminosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TerminosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TerminosPage');
    };
    TerminosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-terminos',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/terminos/terminos.html"*/'<!--\n  Generated template for the FormularioPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title class="nav-color"><img class="logo-header" src="../../assets/imgs/EasyParking-logo.png" alt="">Easy Parking</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-grid>\n\n\n		<ion-card class="cards-list-demo card-terminos">\n			<ion-row>\n				<ion-col  col-sm-10 offset-2 col-md-10 offset-2 col-lg-10 offset-2>\n					<span class="span-terminos">Términos y Condiciones</span>\n\n				</ion-col>\n			</ion-row>\n			<span class="condiciones">Alquiler</span>\n			<p class="parrafo-condiciones" >\n\n				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere obcaecati culpa, ex sed molestiae illum qui consequatur veniam dicta modi ea vel dolores saepe accusamus rerum minus. Voluptatem accusamus, dolorem.\n			</p><br>\n\n			<span class="condiciones">Derechos de propietario</span>\n			<p class="parrafo-condiciones" >\n\n				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere obcaecati culpa, ex sed molestiae illum qui consequatur veniam dicta modi ea vel dolores saepe accusamus rerum minus. Voluptatem accusamus, dolorem.\n			</p><br>\n			<span class="condiciones">Cobranzas</span>\n			<p class="parrafo-condiciones" >\n\n				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere obcaecati culpa, ex sed molestiae illum qui consequatur veniam dicta modi ea vel dolores saepe accusamus rerum minus. Voluptatem accusamus, dolorem.\n			</p>\n			<div>\n			<button class="butt-login-1" ion-button ful color="danger" block>Aceptar</button>\n		</div>\n		</ion-card>\n		\n		<!-- fin de lacard -->\n\n\n	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/terminos/terminos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], TerminosPage);
    return TerminosPage;
}());

//# sourceMappingURL=terminos.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(232);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_registro_registro__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_restaurar_password_restaurar_password__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_ranking_ranking__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_scanner_scanner__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_cerca_de_mi_cerca_de_mi__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_metodo_pago_metodo_pago__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_select_tiempo_select_tiempo__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_agradecimiento_agradecimiento__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_terminos_terminos__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_formulario_formulario__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_mis_publicaciones_mis_publicaciones__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_mis_alquileres_mis_alquileres__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_tab_icon_text_content_tab_icon_text_content__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_login_provider_login_provider__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_registro_provider_registro_provider__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_agradecimiento_provider_agradecimiento_provider__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_cerca_de_mi_provider_cerca_de_mi_provider__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_formulario_provider_formulario_provider__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_pago_provider_pago_provider__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_alquileres_provider_alquileres_provider__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_publicaciones_provider_publicaciones_provider__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_ranking_provider_ranking_provider__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_restaurar_password_provider_restaurar_password_provider__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_scanner_provider_scanner_provider__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_select_tiempo_provider_select_tiempo_provider__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__providers_terminos_provider_terminos_provider__ = __webpack_require__(300);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// import { Camera } from '@ionic-native/camera';















// import  Provider;













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_registro_registro__["a" /* RegistroPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_restaurar_password_restaurar_password__["a" /* RestaurarPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_ranking_ranking__["a" /* RankingPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_scanner_scanner__["a" /* ScannerPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cerca_de_mi_cerca_de_mi__["a" /* CercaDeMiPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_metodo_pago_metodo_pago__["a" /* MetodoPagoPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_select_tiempo_select_tiempo__["a" /* SelectTiempoPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_agradecimiento_agradecimiento__["a" /* AgradecimientoPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_terminos_terminos__["a" /* TerminosPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_formulario_formulario__["a" /* FormularioPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_mis_publicaciones_mis_publicaciones__["a" /* MisPublicacionesPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_mis_alquileres_mis_alquileres__["a" /* MisAlquileresPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_tab_icon_text_content_tab_icon_text_content__["a" /* TabIconTextContentPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/agradecimiento/agradecimiento.module#AgradecimientoPageModule', name: 'AgradecimientoPage', segment: 'agradecimiento', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cerca-de-mi/cerca-de-mi.module#CercaDeMiPageModule', name: 'CercaDeMiPage', segment: 'cerca-de-mi', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/formulario/formulario.module#FormularioPageModule', name: 'FormularioPage', segment: 'formulario', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/metodo-pago/metodo-pago.module#MetodoPagoPageModule', name: 'MetodoPagoPage', segment: 'metodo-pago', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ranking/ranking.module#RankingPageModule', name: 'RankingPage', segment: 'ranking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/registro/registro.module#RegistroPageModule', name: 'RegistroPage', segment: 'registro', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/restaurar-password/restaurar-password.module#RestaurarPasswordPageModule', name: 'RestaurarPasswordPage', segment: 'restaurar-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/scanner/scanner.module#ScannerPageModule', name: 'ScannerPage', segment: 'scanner', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/select-tiempo/select-tiempo.module#SelectTiempoPageModule', name: 'SelectTiempoPage', segment: 'select-tiempo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/terminos/terminos.module#TerminosPageModule', name: 'TerminosPage', segment: 'terminos', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_registro_registro__["a" /* RegistroPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_restaurar_password_restaurar_password__["a" /* RestaurarPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_ranking_ranking__["a" /* RankingPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_scanner_scanner__["a" /* ScannerPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cerca_de_mi_cerca_de_mi__["a" /* CercaDeMiPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_metodo_pago_metodo_pago__["a" /* MetodoPagoPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_select_tiempo_select_tiempo__["a" /* SelectTiempoPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_agradecimiento_agradecimiento__["a" /* AgradecimientoPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_terminos_terminos__["a" /* TerminosPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_formulario_formulario__["a" /* FormularioPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_mis_publicaciones_mis_publicaciones__["a" /* MisPublicacionesPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_mis_alquileres_mis_alquileres__["a" /* MisAlquileresPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_tab_icon_text_content_tab_icon_text_content__["a" /* TabIconTextContentPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                // Camera,
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_20__providers_login_provider_login_provider__["a" /* LoginProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_registro_provider_registro_provider__["a" /* RegistroProvider */],
                __WEBPACK_IMPORTED_MODULE_22__providers_agradecimiento_provider_agradecimiento_provider__["a" /* AgradecimientoProvider */],
                __WEBPACK_IMPORTED_MODULE_23__providers_cerca_de_mi_provider_cerca_de_mi_provider__["a" /* CercaDeMiProvider */],
                __WEBPACK_IMPORTED_MODULE_24__providers_formulario_provider_formulario_provider__["a" /* FormularioProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_pago_provider_pago_provider__["a" /* PagoProvider */],
                __WEBPACK_IMPORTED_MODULE_26__providers_alquileres_provider_alquileres_provider__["a" /* AlquileresProvider */],
                __WEBPACK_IMPORTED_MODULE_27__providers_publicaciones_provider_publicaciones_provider__["a" /* PublicacionesProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_ranking_provider_ranking_provider__["a" /* RankingProvider */],
                __WEBPACK_IMPORTED_MODULE_29__providers_restaurar_password_provider_restaurar_password_provider__["a" /* RestaurarPasswordProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_scanner_provider_scanner_provider__["a" /* ScannerProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_select_tiempo_provider_select_tiempo_provider__["a" /* SelectTiempoProvider */],
                __WEBPACK_IMPORTED_MODULE_32__providers_terminos_provider_terminos_provider__["a" /* TerminosProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the LoginServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LoginProvider = /** @class */ (function () {
    function LoginProvider(http) {
        this.http = http;
        console.log('Hello LoginProvider Provider');
    }
    LoginProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], LoginProvider);
    return LoginProvider;
}());

//# sourceMappingURL=login-provider.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistroProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RegistroServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RegistroProvider = /** @class */ (function () {
    function RegistroProvider(http) {
        this.http = http;
        console.log('Hello RegistroServiceProvider Provider');
    }
    RegistroProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RegistroProvider);
    return RegistroProvider;
}());

//# sourceMappingURL=registro-provider.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgradecimientoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the AgradecimientoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AgradecimientoProvider = /** @class */ (function () {
    function AgradecimientoProvider(http) {
        this.http = http;
        console.log('Hello AgradecimientoServiceProvider Provider');
    }
    AgradecimientoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], AgradecimientoProvider);
    return AgradecimientoProvider;
}());

//# sourceMappingURL=agradecimiento-provider.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CercaDeMiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the CercaDeMiServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var CercaDeMiProvider = /** @class */ (function () {
    function CercaDeMiProvider(http) {
        this.http = http;
        console.log('Hello CercaDeMiServiceProvider Provider');
    }
    CercaDeMiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], CercaDeMiProvider);
    return CercaDeMiProvider;
}());

//# sourceMappingURL=cerca-de-mi-provider.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormularioProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the FormularioServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var FormularioProvider = /** @class */ (function () {
    function FormularioProvider(http) {
        this.http = http;
        console.log('Hello FormularioServiceProvider Provider');
    }
    FormularioProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], FormularioProvider);
    return FormularioProvider;
}());

//# sourceMappingURL=formulario-provider.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the PagoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var PagoProvider = /** @class */ (function () {
    function PagoProvider(http) {
        this.http = http;
        console.log('Hello PagoServiceProvider Provider');
    }
    PagoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], PagoProvider);
    return PagoProvider;
}());

//# sourceMappingURL=pago-provider.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlquileresProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the AlquileresServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AlquileresProvider = /** @class */ (function () {
    function AlquileresProvider(http) {
        this.http = http;
        console.log('Hello AlquileresServiceProvider Provider');
    }
    AlquileresProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], AlquileresProvider);
    return AlquileresProvider;
}());

//# sourceMappingURL=alquileres-provider.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicacionesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the PublicacionesServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var PublicacionesProvider = /** @class */ (function () {
    function PublicacionesProvider(http) {
        this.http = http;
        console.log('Hello PublicacionesServiceProvider Provider');
    }
    PublicacionesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], PublicacionesProvider);
    return PublicacionesProvider;
}());

//# sourceMappingURL=publicaciones-provider.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RankingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RankingServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RankingProvider = /** @class */ (function () {
    function RankingProvider(http) {
        this.http = http;
        console.log('Hello RankingServiceProvider Provider');
    }
    RankingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RankingProvider);
    return RankingProvider;
}());

//# sourceMappingURL=ranking-provider.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurarPasswordProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RestaurarPasswordServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestaurarPasswordProvider = /** @class */ (function () {
    function RestaurarPasswordProvider(http) {
        this.http = http;
        console.log('Hello RestaurarPasswordServiceProvider Provider');
    }
    RestaurarPasswordProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestaurarPasswordProvider);
    return RestaurarPasswordProvider;
}());

//# sourceMappingURL=restaurar-password-provider.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScannerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ScannerServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ScannerProvider = /** @class */ (function () {
    function ScannerProvider(http) {
        this.http = http;
        console.log('Hello ScannerServiceProvider Provider');
    }
    ScannerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ScannerProvider);
    return ScannerProvider;
}());

//# sourceMappingURL=scanner-provider.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectTiempoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the SelectTiempoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SelectTiempoProvider = /** @class */ (function () {
    function SelectTiempoProvider(http) {
        this.http = http;
        console.log('Hello SelectTiempoServiceProvider Provider');
    }
    SelectTiempoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], SelectTiempoProvider);
    return SelectTiempoProvider;
}());

//# sourceMappingURL=select-tiempo-provider.js.map

/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TerminosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the TerminosServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var TerminosProvider = /** @class */ (function () {
    function TerminosProvider(http) {
        this.http = http;
        console.log('Hello TerminosServiceProvider Provider');
    }
    TerminosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], TerminosProvider);
    return TerminosProvider;
}());

//# sourceMappingURL=terminos-provider.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__formulario_formulario__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__restaurar_password_restaurar_password__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    LoginPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__formulario_formulario__["a" /* FormularioPage */]);
    };
    LoginPage.prototype.resetPass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__restaurar_password_restaurar_password__["a" /* RestaurarPasswordPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/login/login.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Login\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <img src="../../assets/imgs/img-EasyParking-1.png"/>\n  <form>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-6>\n            <button ion-button outline icon-start>\n              <img class="ico-face" src="../../assets/imgs/svg/002-facebook.svg" alt="">\n              <p class="text-face">INGRESA CON <br> <span class="text-span-face">Facebook</span></p> \n            </button>\n          </ion-col>\n          <ion-col col-6>\n            <button ion-button outline icon-start>\n              <img class="ico-face" src="../../assets/imgs/svg/001-search.svg" alt="">\n              <p class="text-google">INGRESA CON <br> <span class="text-span-google">Google</span></p> \n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n\n    <ion-list inset>\n       <ion-item>\n        <ion-icon class="ico-mail" name="mail" item-start></ion-icon>\n        <ion-label floating>Correo</ion-label>\n        <ion-input type="text"  required></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-icon class="ico-lock" name="lock" item-start></ion-icon>\n        <ion-label floating>Contraseña</ion-label>\n        <ion-input type="Password"  required></ion-input>\n      </ion-item>\n\n    </ion-list>\n\n    <div>\n     <button class="butt-login-1" ion-button ful color="primary" block (click)="login()">INICIAR SESIÓN</button>\n     <button (click)="resetPass()" class="butt-login-2" ion-button ful color="secondary" block><small>Olvidé mi contraseña</small></button>\n   </div>\n</form>\n</ion-content>\n'/*ion-inline-end:"/home/analista01/Escritorio/proyectoEasyPark/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[211]);
//# sourceMappingURL=main.js.map